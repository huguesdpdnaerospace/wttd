# Jeu console "Welcome to the Dungeon" #
Ce dépôt a été créé dans le but de review le code par des dévelopeurs professionnels écrit par Mayeul et Arthur pour le projet Wttd dans le cadre de leurs études pour l'ESTACA.


### Prérequis ###
- Avoir WSL activé
    - Activer ou désactiver des fonctionnalités de Windows
    - Cocher la case `Sous-système Windows pour Linux`
    - Redémarrer Windows
- Avoir Ubuntu installé
    - Ouvrir le Microsoft Store
    - Rechercher Ubuntu
    - Installer `Ubuntu` ou `Ubuntu 20.04 LTS`
- Avoir Ubuntu configuré
    - Lancer WSL (rechercher dans le menu démarrer)
    - Ajouter WSL à la barre des tâches
    - Ajouter un nom d'utilisateur UNIX (par exemple votre prénom)
    - Définir un mot de passe pour cet utilisateur
    - Mettre à jour le système : `sudo apt-get update && sudo apt-get upgrade -y`
    - Installer les programmes necéssaires `sudo apt-get install vim nano emacs htop gcc valgrind tree make git curl wget unzip zip tar`
- Avoir WSL d'ouvert
    - Ouvrir WSL (menu démarrer ou barre de tâches)

### Installation et utilisation ###
Allez dans votre home:

`cd ~`



Clonez le projet sur votre machine

`git clone git@bitbucket.org:dupoug_h/wttd.git Wttd`



Allez dans le dossier que vous venez de télécharger
`cd Wttd`


### Pour y jouer ###
Compilez le projet `make`
Jouez en lançant le jeu comme suit: `./Wttd`


### Pour développer ###
Le projet comporte deux dossiers:
`sources`  pour les sources (fichiers en \*c)
`includes`  pour les headers (fichiers en \*.h)

Aller dans le dossier GIT:
`cd ~/Wttd`

La compilation se fait avec le script suivant: `./compile.sh`

- La compilation se fera avec `gcc -g3 -Wextra -Wall -Werror *.c`

- L'exécution se fera avec `valgrind ./Wttd`

Ce système permet de tester et d'avoir un programme avec le minimum d'erreurs possibles


### Contributions ###
- Seuls Mayeul ou Arthur peuvent directement apporter des modifications à leur propre code
- Toute personne extérieur ou étudiant de l'ESTACA est libre de soumettre sa pull request (PR) pour review

### Erreur fréquentes que je vois ###
- Le code d'écrit en ANGLAIS (et pas en français...)
- Les structures se déclarent ainsi:

`typedef struct s_xxxxxxxx`

`{`

`int myAttribute;`

`} xxxxxxxx;`

- Un header se définit comme suit:

`#ifndef MY_FILE_H`

`#define MY_FILE_H`

`...`

`#endif /* MY_FILE_H */`

### À faire ###
- Blamer le professeur pour écrire ses codes d'exemple en français
- Blamer le professeur pour demander aux étudiants de faire le projet sur CodeBlocks
- Blamer le professeur pour demander aux étudiants d'utiliser des fonctions deprecated (`gets`, etc.)